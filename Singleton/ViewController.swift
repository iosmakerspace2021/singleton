//
//  ViewController.swift
//  Singleton
//
//  Created by Olzhas Akhmetov on 23.04.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func saveData(_ sender: Any) {
        Storage.sharedInstance.someText = textField.text!
    }
    
}

