//
//  Storage.swift
//  Singleton
//
//  Created by Olzhas Akhmetov on 23.04.2021.
//

import Foundation

class Storage {
    public var someText: String = ""
    
    static let sharedInstance = Storage()
    
}
